
class Aurhorization // Класс "Авторизация"
{
public:
	void Aurhorization(); // Конструктор
	void ~Aurhorization(); // Диструктор 
	void Control(); // Метод "Переключения ролей пользователей"
private:
	char Login; // Поле "Логин"
	char Password; // Поле "Пароль"
}; 

class Materials // Класс "Материалы"
{
public:
	void Materials(); // Конструктор
	void ~Materials(); // Диструктор
private:
	char M_name; // Поле "Наименование материала"
	double M_lenghth; // Поле "Длина"
	double M_width; // Поле "Ширина"
	double M_costList; // Поле "Цена за лист"

};

class Fittings // Класс "Фурнитура"
{
public:
	void Fittings(); // Конструктор
	void ~Fittings(); // Диструктор
private:
	char F_name; // Поле "Наименование"
	int F_count; // Поле "Количество"
	double F_cost; // Поле "Стоимость"
};

class Details // Класс "Детали"
{
public:
	void Details(); // Конструктор
	void ~Details(); // Диструктор
private:
	char D_name; // Поле "Наименование"
	double D_lenghth; // Поле "Длина
	double D_width; // Поле "Ширина"
	int D_count; // Поле "Количество"
};

class Order // Класс "Заказ"
{
public:
	void Order(); // Конструктор
	void ~Order(); // Диструктор
private:
	char O_inviteDate; // Поле "Дата Заказа"
	char O_name; // Поле "Наименование"
	int O_komplectCount; // Поле "Количество комплектов"
	char O_executionPeriod; // Поле "Период сборки"
};

class Customer // Класс "Клиент"
{
public:
	void Customer(); // Конструктор
	void ~Customer(); // Диструктор
private:
	char C_fio; // Поле "ФИО"
	char C_address; // Поле "Адрес"
	char C_phone; // Поле "Телефон"
};

class Department // Класс "Отдел"
{
public:
	void Department(); // Конструктор
	void ~Department(); // Диструктор
private:
	char Dep_name; // Поле "Наименование отдела"
};

class Employee // Класс "Сотрудник"
{
public:
	void Employee(); // Конструктор
	void ~Employee(); // Диструктор
private:
	char E_fio; // Поле "ФИО"
	char E_departmentName; // Поле "Наименование отдела"
	char E_PasportNumber; // Поле "Номер паспорта"
	char E_employmentDate; // Поле "Дата найма"
	char E_familyStatus; // Поле "Семейное положение"
	char E_position; // Поле "Должность"
	double E_salary; // Поле "Зарплата"
};

class Invoice // Класс "Счет"
{
public:
	void Invoice(); // Конструктор
	void ~Invoice(); // Диструктор
private:
	char I_date; // Поле "Дата счета"
	double I_materialsSum; // Поле "Стоимость материалов"
	double I_workSum; // Поле "Стоимость работы"
	double I_totalSum; // Поле "Итоговая сумма"
};

class Work_Complete_Akt // Класс "Акт о выполненной работе"
{
public:	
	void Work_Complete_Akt(); // Конструктор
	void ~Work_Complete_Akt(); // Диструктор
private:
	char WCA_endWork; // Поле "Дата окончания работы"
	char WCA_fioWorker; // Поле "ФИО рабочего"
};
